module.exports = {
  publicPath:
    process.env.NODE_ENV === 'production'
      ? '/~cs62160255/learn_bootstrap/'
      : '/'
}
